import { fullName } from '../service/service'

test('fullName ', () => {
  let person = {
    firstName: 'Adrian',
    lastName: 'Zubieta'
  }
  expect(fullName(person)).toBe('Adrian Zubieta');
});