import { createConnection } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import logger from './logger';

export default async () => {
  try {
    const options = {
      type: 'mysql',
      host: 'localhost',
      username: 'test',
      port: 3306,
      password: 'test',
      database: 'test',
      synchronize: true ,
      entities: [
        'src/entity/*.js',
      ],
      namingStrategy: new SnakeNamingStrategy(),
    };
    await createConnection(options);
  } catch (ex) {
    logger.error('Cannot connect database: ', ex);
  }
};
